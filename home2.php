<?php 

/* Template Name: Home 2 */
get_header(); 

?>

	<div class="main-container">
	
		<main class="site-main">
			
			<!-- Slider Section -->
			<div class="container-fluid no-left-padding no-right-padding slider-section slider-section2">
				<!-- Container -->
				<div class="container">
					<div id="slider-carousel-2" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="carousel-item active">
								<div class="row">
							<?php 
								$args = array(
									'posts_per_page' => 3,
									'post__in' => get_option( 'sticky_posts' ),
									'category_name' => 'slider',
									'ignore_sticky_posts' => 1
							); 
								$wp_query = new WP_Query($args); 
							if ( $wp_query->have_posts() ) : 
								while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
									<div class="col-lg-8 col-sm-12 post-block post-big">
										<div class="post-box">
											<?php the_post_thumbnail();?>
											<div class="entry-content">
											<span class="post-category" ><a><?php the_category(' ');?></a></span>
												<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
												<a href="<?php the_permalink();?>">Read More</a>
											</div> 
										</div>
		                       </div>

									<?php 
									// $cata =get_the_category();
									// print_r($cata);
								endwhile; 
									wp_reset_postdata(); else : ?>
										<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
									<?php endif; ?>

									<div class="col-lg-4 col-sm-12 post-block post-thumb">

						<?php 
								$args = array(
									'posts_per_page' => 2,
									'ignore_sticky_posts' => true
							); 
								$wp_query = new WP_Query($args); 
							if ( $wp_query->have_posts() ) : 
								while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
										<div class="post-box">
										<?php the_post_thumbnail();?>
											<div class="entry-content">
												<span class="post-category"><a><?php the_category(' ');?></a></span>
												<h3><a href="<?php the_permalink();?>" ><?php the_title();?></a></h3>
												<a href="<?php the_permalink();?>">Read More</a>
											</div>
						<?php endwhile; 
							wp_reset_postdata(); else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>

										</div>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<div class="row">
									<div class="col-lg-8 post-block post-big">
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3><a href="#" title="Best Surfing Spots for Beginners and Advanced">Best Surfing Spots for Beginners and Advanced</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
									</div>
									<div class="col-lg-4 post-block post-thumb">
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Sport">Sport</a></span>
												<h3><a href="#" title="High-Tech Prototype Bike Announced">High-Tech Prototype Bike Announced</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Nature">Nature</a></span>
												<h3><a href="#" title="White Sand of The Desert Discovery">White Sand of The Desert Discovery</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Slider Section /- -->
			
			<!-- Page Content -->
			<div class="container-fluid no-left-padding no-right-padding page-content blog-paralle-post">
				<!-- Container -->
				<div class="container">
					<!-- Row -->
					<div class="row">
						<!-- Content Area -->
						<div class="col-lg-8 col-md-6 content-area">
							<!-- Row -->
							<div class="row">

					  <?php 
							$args = array('post_type'=>'post', 
							'posts_per_page'=> 6,
						);
							$wp_query = new WP_Query($args); 
						  if ( $wp_query->have_posts() ) : 
							  while ( $wp_query->have_posts() ) : $wp_query->the_post();?>

								<div class="col-12 col-md-12 col-sm-6 blog-paralle">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"" title="<?php the_author(); ?>"><?php the_author(); ?></a></span>
												<span class="post-date"><a href="#"><?php echo get_the_date(); ?></a></span>
											</div>
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">
											<?php foreach (get_the_category() as $category): ?>
												<span class="post-category"><a href="<?php echo get_category_link($category->cat_ID) ?>" title="<?php echo $category->cat_name ?>"><?php echo $category->cat_name ?></a></span>
											<?php endforeach; ?>
												
												<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
											</div>								
											<?php the_excerpt(); ?>
											<a href="<?php the_permalink(); ?>" title="Read More">Read More</a>
										</div>
									</div>
								</div>

                          <?php endwhile; 
							wp_reset_postdata(); else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>

							</div><!-- Row /- -->

						</div><!-- Content Area -->
						<!-- Widget Area -->
						<?php get_sidebar();?>
					</div><!-- Row /- -->
				</div><!-- Container /- -->
			</div><!-- Page Content /- -->
			
		</main>
		
	</div>
	
<?php get_footer();?>