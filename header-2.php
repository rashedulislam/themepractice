<!DOCTYPE html>
<html <?php language_attributes();?>>  
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="pingback" href="<?php bloginfo('pingback_url');?>" />
    
    <!--  Favicons  -->
    <link rel="icon" type="image/x-icon" href="<?php echo get_theme_file_uri('/assets/images/favicon.ico') ?>" />

    <!-- For iPhone 4 Retina display: -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_theme_file_uri('/assets/images/apple-touch-icon-114x114-precomposed.png') ?>">
	
	<!-- For iPad: -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_theme_file_uri('/assets/images/apple-touch-icon-72x72-precomposed.html') ?>">
	
	<!-- For iPhone: -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_theme_file_uri('/assets/images/apple-touch-icon-57x57-precomposed.png') ?>">	
	
	<!-- Library - Google Font Familys -->
	<link href="<?php echo get_theme_file_uri('../../../../fonts.googleapis.com/css8896.css?family=Hind:300,400,500,600,700%7cMontserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i') ?>" rel="stylesheet">
	
	
	<link rel="stylesheet" type="text/css" href="assets/revolution/css/settings.css">
	<!-- Library -->
    <link href="assets/css/lib.css" rel="stylesheet">
 
    <?php wp_head();?>
 </head>
 <body <?php body_class(); ?> data-offset="200" data-spy="scroll" data-target=".ownavigation">

    <div id="site-loader" class="load-complete">
		<div class="loader">
			<div class="line-scale"><div></div><div></div><div></div><div></div><div></div></div>
		</div>
    </div><!-- Loader /- -->
    
    <header class="container-fluid no-left-padding no-right-padding header_s header-fix header_s3">
		<!-- Top Header -->
		<div class="container-fluid no-left-padding no-right-padding top-header">
			<!-- Container -->
			<div class="container">

                <?php
                    wp_nav_menu( array(
                        'theme_location'  => 'signin',    
					) ); ?>
					
			</div><!-- Container /- -->
		</div><!-- Top Header /- -->
		<!-- Menu Block -->
		<div class="container-fluid no-left-padding no-right-padding menu-block">
			<!-- Container -->
			<div class="container">			
				<nav class="navbar ownavigation navbar-expand-lg">
					<a class="navbar-brand" href="<?php echo site_url(); ?>">Mini :: Blog</a>
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar4" aria-controls="navbar4" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fa fa-bars"></i>
					</button>

					<?php 
          
                        wp_nav_menu( array(
                                'theme_location'  => 'navbar',
                                'depth'           => 3,
                                'container'       => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id'    => 'navbar4',
                                'menu_class'      => 'navbar-nav justify-content-end',
                                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'          => new WP_Bootstrap_Navwalker(),
							) ); ?>
							
					<ul class="user-info">
						<li><a href="#search-box4" data-toggle="collapse" class="search collapsed" title="Search"><i class="pe-7s-search sr-ic-open"></i><i class="pe-7s-close sr-ic-close"></i></a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" href="#"><i class="pe-7s-user"></i></a>

							<?php
                                   wp_nav_menu( array(
                                     'theme_location'  => 'headsearch',
                                     'container'       => '',
                                     'menu_class'      => 'dropdown-menu',
                                     'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                     'walker'          => new WP_Bootstrap_Navwalker(),
                
                           ) ); ?>
							<!-- <ul class="dropdown-menu">
								<li><a class="dropdown-item" href="#" title="Sign In">Sign In</a></li>
								<li><a class="dropdown-item" href="#" title="Subscribe">Subscribe </a></li>
								<li><a class="dropdown-item" href="#" title="Log In">Log In</a></li>
							</ul> -->
						</li>
					</ul>
				</nav>
			</div><!-- Container /- -->
		</div><!-- Menu Block /- -->
		<!-- Search Box -->
		<div class="search-box collapse" id="search-box4">
			<div class="container">
				<form>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search..." required>
						<span class="input-group-btn">
							<button class="btn btn-secondary" type="submit"><i class="pe-7s-search"></i></button>
						</span>
					</div>
				</form>
			</div>
		</div><!-- Search Box /- -->
	</header><!-- Header Section /- -->