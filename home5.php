<?php 

/* Template Name: Home 5 */ 
get_header('2');

?>

<div class="main-container">
	
		<main class="site-main">

			<!-- Slider Section -->
			<div class="container-fluid no-left-padding no-right-padding slider-section">
				<div class="slider-carousel-4">
					<div class="post-box">
						<img src="http://via.placeholder.com/472x495" alt="Slide" />
						<div class="entry-content">
							<span class="post-category"><a href="#" title="Technology">Technology</a></span>
							<h3><a href="#" title="Cars With Advanced Safety Systems">Cars With Advanced Safety Systems</a></h3>
							<a href="#" title="Read more">Read more</a>
						</div>
					</div>
					<div class="post-box">
						<img src="http://via.placeholder.com/472x495" alt="Slide" />
						<div class="entry-content">
							<span class="post-category"><a href="#" title="Travel">Travel</a></span>
							<h3><a href="#" title="Take Great Photography Trip to Spain">Take Great Photography Trip to Spain</a></h3>
							<a href="#" title="Read more">Read more</a>
						</div>
					</div>
					<div class="post-box">
						<img src="http://via.placeholder.com/472x495" alt="Slide" />
						<div class="entry-content">
							<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
							<h3><a href="#" title="Organize Your Life with 10 Simple Rules">Organize Your Life with 10 Simple Rules</a></h3>
							<a href="#" title="Read more">Read more</a>
						</div>
					</div>
					<div class="post-box">
						<img src="http://via.placeholder.com/472x495" alt="Slide" />
						<div class="entry-content">
							<span class="post-category"><a href="#" title="Fashion">Fashion</a></span>
							<h3><a href="#" title="Independent Fashion Show">Independent Fashion Show</a></h3>
							<a href="#" title="Read more">Read more</a>
						</div>
					</div>
				</div>
			</div><!-- Slider Section /- -->
			
			<!-- Trending Section -->
			<div class="container-fluid no-left-padding no-right-padding trending-section">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Trending</h3>
					</div><!-- Section Header /- -->
					<div class="trending-carousel">
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Art">Art</a></span>
									<h3 class="entry-title"><a href="#">A penguin bicycled behind an escalator</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Travel">TRAVEL</a></span>
									<h3 class="entry-title"><a href="#">There was a legend about the well in the garden</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Nature">NATURE</a></span>
									<h3 class="entry-title"><a href="#">The entrance to the tunnel was his only way out</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Nature">NATURE</a></span>
									<h3 class="entry-title"><a href="#">He was going back to a place he'd hoped</a></h3>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Trending Section /- -->
			<!-- Page Content -->
			<div class="container-fluid no-left-padding no-right-padding page-content">
				<!-- Container -->
				<div class="container">
					<div class="row">
						<!-- Content Area -->
						<div class="col-lg-8 col-md-6 content-area">
							<!-- Row -->
							<div class="row">
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Technology">Technology</a></span>
												<h3 class="entry-title"><a href="#" title="Traffic Jams Solved">Traffic Jams Solved </a></h3>
											</div>								
											<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Charming Evening Field">Charming Evening Field</a></h3>
											</div>								
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Boat Trip to Mediterranean">Boat Trip to Mediterranean</a></h3>
											</div>								
											<p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Nature">Nature</a></span>
												<h3 class="entry-title"><a href="#" title="Cliff Sunset Sea View">Cliff Sunset Sea View</a></h3>
											</div>								
											<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="entry-title"><a href="#" title="Trendy Summer Fashion">Trendy Summer Fashion</a></h3>
											</div>								
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Hammock Camping Tips">Hammock Camping Tips</a></h3>
											</div>								
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Beautiful Rio de Janeiro">Beautiful Rio de Janeiro</a></h3>
											</div>								
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="entry-title"><a href="#" title="New Fashion Outfits">New Fashion Outfits</a></h3>
											</div>								
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
							</div><!-- Row /- -->
							<!-- Pagination -->
							<nav class="navigation pagination">
								<h2 class="screen-reader-text">Posts navigation</h2>
								<div class="nav-links">
									<a class="prev page-numbers" href="#">Previous</a>
									<span class="page-numbers current">
										<span class="meta-nav screen-reader-text">Page </span>1
									</span>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>2</a>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>3</a>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>...</a>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>6</a>
									<a class="next page-numbers" href="#">Next</a>
								</div>
							</nav><!-- Pagination /- -->
						</div><!-- Content Area /- -->
						<!-- Widget Area -->
						<div class="col-lg-4 col-md-6 widget-area">
							<!-- Widget : Latest Post -->
							<aside class="widget widget_latestposts">
								<h3 class="widget-title">Popular Posts</h3>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="Beautiful Landscape View of Rio de Janeiro" href="#">Beautiful Landscape View of Rio de Janeiro</a></h5>
									<span><a href="#">march 14, 2017</a></span>
								</div>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="Enjoy Your Holiday with Adventures" href="#">Enjoy Your Holiday with Adventures</a></h5>
									<span><a href="#">march 15, 2017</a></span>
								</div>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="Best Photography Experience Shooting" href="#">Best Photography Experience Shooting</a></h5>
									<span><a href="#">march 15, 2017</a></span>									
								</div>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="How to Forecast Your Retirement Savings" href="#">How to Forecast Your Retirement Savings</a></h5>
									<span><a href="#">march 16, 2017</a></span>									
								</div>
							</aside><!-- Widget : Latest Post /- -->
							<!-- Widget : Categories -->
							<aside class="widget widget_categories text-center">
								<h3 class="widget-title">Categories</h3>
								<ul>
									<li><a href="#" title="Nature">Nature</a></li>
									<li><a href="#" title="Technology">Technology</a></li>
									<li><a href="#" title="Travel">Travel</a></li>
									<li><a href="#" title="Sport">Sport</a></li>
									<li><a href="#" title="Lifestyle">Lifestyle</a></li>
								</ul>
							</aside><!-- Widget : Categories /- -->
							<!-- Widget : Instagram -->
							<aside class="widget widget_instagram">
								<h3 class="widget-title">Instagram</h3>
								<ul>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
								</ul>
							</aside><!-- Widget : Instagram /- -->
							<!-- Widget : Follow Us -->
							<aside class="widget widget_social">
								<h3 class="widget-title">FOLLOW US</h3>
								<ul>
									<li><a href="#" title=""><i class="ion-social-facebook-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-twitter-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-instagram-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-pinterest-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-vimeo-outline"></i></a></li>
								</ul>
							</aside><!-- Widget : Follow Us /- -->
							<!-- Widget : Newsletter -->
							<aside class="widget widget_newsletter">
								<h3 class="widget-title">Newsletter</h3>
								<div class="newsletter-box">
									<i class="ion-ios-email-outline"></i>
									<h4>Sign Up for Newsletter</h4>
									<p>Sign up to receive latest posts and news </p>
									<form>
										<input type="text" class="form-control" placeholder="Your email address" />
										<input type="submit" value="Subscribe Now" />
									</form>
								</div>
							</aside><!-- Widget : Newsletter /- -->
							<!-- Widget : Tags -->
							<aside class="widget widget_tags_cloud">
								<h3 class="widget-title">Tags</h3>
								<div class="tagcloud">
									<a href="#" title="Fashion">Fashion</a>
									<a href="#" title="Travel">Travel</a>
									<a href="#" title="Nature">Nature</a>
									<a href="#" title="Technology">Technology</a>
									<a href="#" title="Sport">Sport</a>
									<a href="#" title="Weather">Weather</a>
									<a href="#" title="Trends">Trends</a>
									<a href="#" title="Lifestyle">Lifestyle</a>
									<a href="#" title="Gear">Gear</a>
								</div>
							</aside><!-- Widget : Tags /- -->
						</div><!-- Widget Area /- -->
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Content /- -->
			
		</main>
		
	</div>

<?php get_footer();?>