<div class="col-lg-4 col-md-6 widget-area">
    <?php dynamic_sidebar('sidebar-1')?>
    
    <!-- Widget : Instagram -->
    <aside class="widget widget_instagram">
        <h3 class="widget-title">Instagram</h3>
        <ul>
        <?php 
							$args = array('post_type'=>'post', 'posts_per_page'=> 6); 
							$wp_query = new WP_Query($args); 
						  if ( $wp_query->have_posts() ) : 
							  while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
                
            
              <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a></li>

              <?php endwhile; 
							wp_reset_postdata(); else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>
            <!-- <li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
            <li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
            <li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
            <li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
            <li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
            <li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li> -->
        </ul>
    </aside><!-- Widget : Instagram /- -->
    <!-- Widget : Follow Us -->
    <aside class="widget widget_social">
        <h3 class="widget-title">FOLLOW US</h3>
        <?php
        wp_nav_menu( array(
            'theme_location'  => 'socailsidebar',
        ) ); ?>
        <!-- <ul>
            <li><a href="#" title=""><i class="ion-social-facebook-outline"></i></a></li>
            <li><a href="#" title=""><i class="ion-social-twitter-outline"></i></a></li>
            <li><a href="#" title=""><i class="ion-social-instagram-outline"></i></a></li>
            <li><a href="#" title=""><i class="ion-social-googleplus-outline"></i></a></li>
            <li><a href="#" title=""><i class="ion-social-pinterest-outline"></i></a></li>
            <li><a href="#" title=""><i class="ion-social-vimeo-outline"></i></a></li>
        </ul> -->
    </aside><!-- Widget : Follow Us /- -->
    <!-- Widget : Newsletter -->
    <aside class="widget widget_newsletter">
        <h3 class="widget-title">Newsletter</h3>
        <div class="newsletter-box">
            <i class="ion-ios-email-outline"></i>
            <h4>Sign Up for Newsletter</h4>
            <p>Sign up to receive latest posts and news </p>
            <form>
                <input type="text" class="form-control" placeholder="Your email address" />
                <input type="submit" value="Subscribe Now" />
            </form>
        </div>
    </aside><!-- Widget : Newsletter /- -->							
</div><!-- Widget Area /- -->