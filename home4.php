<?php 

/* Template Name: Home 4 */
get_header(); 

?>

	<div class="main-container">
	
		<main class="site-main">
			
			<!-- Slider Section -->
			<div class="container-fluid no-left-padding no-right-padding slider-section slider-section3">
				<!-- Container -->
				<div class="container">
					<div id="slider-carousel-3" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="carousel-item active">
								<div class="row">
									<div class="col-lg-8 col-sm-12 post-block post-big">
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3><a href="#" title="Best Surfing Spots for Beginners and Advanced">Best Surfing Spots for Beginners and Advanced</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-sm-12 post-block post-thumb">
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Sport">Sport</a></span>
												<h3><a href="#" title="High-Tech Prototype Bike Announced">High-Tech Prototype Bike Announced</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<div class="row">
									<div class="col-lg-8 col-sm-12 post-block post-big">
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3><a href="#" title="Best Surfing Spots for Beginners and Advanced">Best Surfing Spots for Beginners and Advanced</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-sm-12 post-block post-thumb">
										<div class="post-box">
											<img src="http://via.placeholder.com/770x500" alt="Slide" />
											<div class="entry-content">
												<span class="post-category"><a href="#" title="Sport">Sport</a></span>
												<h3><a href="#" title="High-Tech Prototype Bike Announced">High-Tech Prototype Bike Announced</a></h3>
												<a href="#" title="Read More">Read More</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Slider Section /- -->
			
			<!-- Trending Section -->
			<div class="container-fluid no-left-padding no-right-padding trending-section">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Trending</h3>
					</div><!-- Section Header /- -->
					<div class="trending-carousel">					
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Art">Art</a></span>
									<h3 class="entry-title"><a href="#">A penguin bicycled behind an escalator</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Travel">TRAVEL</a></span>
									<h3 class="entry-title"><a href="#">There was a legend about the well in the garden</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Nature">NATURE</a></span>
									<h3 class="entry-title"><a href="#">The entrance to the tunnel was his only way out</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Nature">NATURE</a></span>
									<h3 class="entry-title"><a href="#">He was going back to a place he'd hoped</a></h3>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Trending Section /- -->
			
			<!-- Page Content -->
			<div class="container-fluid no-left-padding no-right-padding page-content">
				<!-- Container -->
				<div class="container">
					<div class="row">
						<div class="content-area col-sm-12">
							<!-- Row -->
							<div class="row blog-masonry-list">
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x493" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Charming Evening Field">Charming Evening Field</a></h3>
											</div>								
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="NATURE">NATURE</a></span>
												<h3 class="entry-title"><a href="#" title="Cliff Sunset Sea View">Cliff Sunset Sea View</a></h3>
											</div>
											<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x493" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="entry-title"><a href="#" title="Furniture Layout">Furniture Layout</a></h3>
											</div>
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x370" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="TRAVEL">TRAVEL</a></span>
												<h3 class="entry-title"><a href="#" title="Boat Trip to Mediterranean">Boat Trip to Mediterranean</a></h3>
											</div>
											<p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x493" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Technology">Technology</a></span>
												<h3 class="entry-title"><a href="#" title="Lighthouse Technology">Lighthouse Technology</a></h3>
											</div>
											<p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him.</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Beautiful Rio de Janeiro ">Beautiful Rio de Janeiro </a></h3>
											</div>
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="entry-title"><a href="#" title="Trendy Summer Fashion">Trendy Summer Fashion</a></h3>
											</div>
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x555" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Hammock Camping Tips">Hammock Camping Tips</a></h3>
											</div>
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x370" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="entry-title"><a href="#" title="Furniture Layout">Furniture Layout</a></h3>
											</div>
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x555" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="TRAVEL">TRAVEL</a></span>
												<h3 class="entry-title"><a href="#" title="Winter Outdoor">Traffic Jams Solved</a></h3>
											</div>
											<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x370" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="LIFESTYLE">LIFESTYLE</a></span>
												<h3 class="entry-title"><a href="#" title="Eat More Healthy">Eat More Healthy</a></h3>
											</div>
											<p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="LIFESTYLE">LIFESTYLE</a></span>
												<h3 class="entry-title"><a href="#" title="New Fashion Outfits">New Fashion Outfits</a></h3>
											</div>
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
							</div><!-- Row /- -->
							<!-- Pagination -->
							<nav class="navigation pagination">
								<h2 class="screen-reader-text">Posts navigation</h2>
								<div class="nav-links">
									<a class="prev page-numbers" href="#">Previous</a>
									<span class="page-numbers current">
										<span class="meta-nav screen-reader-text">Page </span>1
									</span>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>2</a>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>3</a>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>...</a>
									<a class="page-numbers" href="#"><span class="meta-nav screen-reader-text">Page </span>6</a>
									<a class="next page-numbers" href="#">Next</a>
								</div>
							</nav><!-- Pagination /- -->
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Content /- -->
			
		</main>
		
	</div>
	
<?php get_footer();?>