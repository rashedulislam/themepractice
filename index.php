<?php get_header(); ?>

	<div class="clearfix"></div>

	<div class="main-container">
	
		<main class="site-main">
			
			<!-- Slider Section -->
			<div class="container-fluid no-left-padding no-right-padding slider-section">
				<div id="mm-slider-1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="mm-slider-1" data-source="gallery">
					<!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
					<div id="mm-slider-1" class="rev_slider fullwidthabanner" data-version="5.4.1">
						<ul>
	
				        <?php 
							$args = array('post_type'=>'post', 
							'posts_per_page'=> 3, 
							'category_name' => 'slider',
							'ignore_sticky_posts' => true
						); 
							$wp_query = new WP_Query($args); 
							$i = 1;
						  if ( $wp_query->have_posts() ) : 
							  while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
	
							<!-- SLIDE  -->
							<li data-index="rs-26" data-transition="random-static,random-premium,random,boxslide,slotslide-horizontal,slotslide-vertical,boxfade,slotfade-horizontal,slotfade-vertical" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default"  data-rotate="0,0,0,0,0,0,0,0,0,0"  data-saveperformance="off"  class="slide-overlay" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
								<!-- MAIN IMAGE -->
								<?php the_post_thumbnail(); ?>
								<!-- LAYERS -->

								<!-- LAYER NR. 1 -->
								<a class="slidecnt1 tp-caption tp-layer-selectable tp-resizeme category-link" href="#" target="_self" rel="nofollow"			 id="slide-26-layer-1" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['-56','-70','-70','-57']" 
									data-fontsize="['14','14','18','18']"
									data-height="none"
									data-whitespace="nowrap"					 
									data-type="text" 
									data-actions=''
									data-responsive_offset="on"
									data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);br:0px 0px 0px 0px;"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"><?php the_category(' '); ?></a>

									<!-- <?php //echo get_the_category(' '); ?> -->

								<!-- LAYER NR. 2 -->
								<a class="slidecnt2 tp-caption tp-layer-selectable tp-resizeme post-title" href="<?php the_permalink(); ?>" target="_self" rel="nofollow" id="slide-26-layer-2" 
									data-x="['center','center','center','center']" data-hoffset="['0','-1','-1','-1']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['6','-5','-5','-5']" 
									data-fontsize="['40','30','30','23']"
									data-lineheight="['40','40','40','30']"
									data-width="['601','601','601','435']"
									data-height="['81','81','81','none']"
									data-whitespace="normal"
						 			data-type="text" 
									data-actions=''
									data-responsive_offset="on" 
									data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-textAlign="['center','center','center','center']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"><?php echo get_the_title(); ?></a>

								<!-- LAYER NR. 3 -->
								<a class="slidecnt3 tp-caption rev-btn tp-layer-selectable" href="<?php the_permalink(); ?>" target="_self" rel="nofollow" id="slide-26-layer-3" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['80','73','73','59']" 
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"						 
									data-type="button" 
									data-actions=''
									data-responsive_offset="on" 
									data-responsive="off"
									data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(21,21,21);bg:rgba(255,255,255,1);"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[2,2,2,2]"
									data-paddingright="[20,20,20,20]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[20,20,20,20]">READ MORE </a>
							</li>
							<?php endwhile; 
							wp_reset_postdata(); else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>
						</ul>
						<div class="tp-bannertimer tp-bottom"></div>
					</div>
				</div><!-- END REVOLUTION SLIDER -->
			</div><!-- Slider Section /- -->
			
			<!-- Page Content -->
			<div class="container-fluid no-left-padding no-right-padding page-content">
				<!-- Container -->
				<div class="container">
					<div class="row">
						<div class="content-area col-sm-12">
							<!-- Row -->
							<div class="row blog-masonry-list">

							<?php 
							$args = array('post_type'=>'post', 
							'posts_per_page'=> 13,
						); 
							$wp_query = new WP_Query($args); 
						  if ( $wp_query->have_posts() ) : 
							  while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
	

								<div class="col-lg-4 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></span>
												<span class="post-date"><a href="#"><?php echo get_the_date(); ?></a></span>
											</div>
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
										</div> 
										<div class="entry-content">
											<div class="entry-header">	

												<span class="post-category"><?php the_category(' '); ?></span>

												<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

											</div>
											<?php the_excerpt(); ?>
											<a href="<?php the_permalink(); ?>" title="Read More">Read More</a>
										</div>
									</div>
								</div>

							<?php endwhile; 
							wp_reset_postdata(); else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>

							</div><!-- Row /- -->
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Content /- -->
			
		</main>
		
	</div> 

<?php get_footer();?>
