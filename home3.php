<?php 

/* Template Name: Home 3 */
get_header(); 

?>

	<div class="main-container">
	
		<main class="site-main">
		
			<!-- Slider Section -->
			<div class="container-fluid no-left-padding no-right-padding slider-section">
				<div id="mm-slider-1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="mm-slider-1" data-source="gallery">
					<!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
					<div id="mm-slider-1" class="rev_slider fullwidthabanner" data-version="5.4.1">
						<ul>	
							<!-- SLIDE  -->
							<li data-index="rs-26" data-transition="random-static,random-premium,random,boxslide,slotslide-horizontal,slotslide-vertical,boxfade,slotfade-horizontal,slotfade-vertical" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default"  data-rotate="0,0,0,0,0,0,0,0,0,0"  data-saveperformance="off"  class="slide-overlay" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
								<!-- MAIN IMAGE -->
								<img src="http://via.placeholder.com/1900x595"  alt="" title="slide-1"  width="1920" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYERS -->

								<!-- LAYER NR. 1 -->
								<a class="slidecnt1 tp-caption tp-layer-selectable tp-resizeme category-link" href="#" target="_self" rel="nofollow"			 id="slide-26-layer-1" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['-56','-70','-70','-57']" 
data-fontsize="['14','14','18','18']"
									data-height="none"
									data-whitespace="nowrap"					 
									data-type="text" 
									data-actions=''
									data-responsive_offset="on"
									data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);br:0px 0px 0px 0px;"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]">TRAVEL </a>

								<!-- LAYER NR. 2 -->
								<a class="slidecnt2 tp-caption tp-layer-selectable tp-resizeme post-title" href="#" target="_self" rel="nofollow" id="slide-26-layer-2" 
									data-x="['center','center','center','center']" data-hoffset="['0','-1','-1','-1']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['6','-5','-5','-5']" 
									data-fontsize="['40','30','30','23']"
									data-lineheight="['40','40','40','30']"
									data-width="['601','601','601','400']"
									data-height="['81','81','81','none']"
									data-whitespace="normal"
						 			data-type="text" 
									data-actions=''
									data-responsive_offset="on" 
									data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-textAlign="['center','center','center','center']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]">Best Surfing Spots for Beginners and Advanced </a>

								<!-- LAYER NR. 3 -->
								<a class="slidecnt3 tp-caption rev-btn tp-layer-selectable" href="#" target="_self" rel="nofollow" id="slide-26-layer-3" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['80','73','73','59']" 
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"						 
									data-type="button" 
									data-actions=''
									data-responsive_offset="on" 
									data-responsive="off"
									data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(21,21,21);bg:rgba(255,255,255,1);"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[2,2,2,2]"
									data-paddingright="[20,20,20,20]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[20,20,20,20]">READ MORE </a>
							</li>
							<!-- SLIDE  -->
							<li data-index="rs-28" data-transition="random-static,random-premium,random,boxslide,slotslide-horizontal,slotslide-vertical,boxfade,slotfade-horizontal,slotfade-vertical" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default"  data-rotate="0,0,0,0,0,0,0,0,0,0"  data-saveperformance="off"  class="slide-overlay" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
								<!-- MAIN IMAGE -->
								<img src="http://via.placeholder.com/1900x595"  alt="" title="slide-1"  width="1920" height="600" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYERS -->

								<!-- LAYER NR. 4 -->
								<a class="slidecnt1 tp-caption tp-layer-selectable tp-resizeme category-link" href="#" target="_self" rel="nofollow"			 id="slide-28-layer-1" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['-56','-70','-70','-57']" 
									data-fontsize="['14','14','18','18']"
									data-height="none"
									data-whitespace="nowrap"						 
									data-type="text" 
									data-actions=''
									data-responsive_offset="on" 
									data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);br:0px 0px 0px 0px;"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]">TRAVEL </a>

								<!-- LAYER NR. 5 -->
								<a class="slidecnt2 tp-caption tp-layer-selectable tp-resizeme post-title" href="#" target="_self" rel="nofollow"			 
								id="slide-28-layer-2" 
									data-x="['center','center','center','center']" data-hoffset="['0','-1','-1','-1']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['6','-5','-5','-5']" 
									data-fontsize="['40','30','30','23']"
									data-lineheight="['40','40','40','30']"
									data-width="['601','601','601','435']"
									data-height="['81','81','81','none']"
									data-whitespace="normal"						 
									data-type="text" 
									data-actions=''
									data-responsive_offset="on" 
									data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-textAlign="['center','center','center','center']"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]">Best Surfing Spots for Beginners	
									and Advanced</a>

								<!-- LAYER NR. 6 -->
								<a class="slidecnt3 tp-caption rev-btn tp-layer-selectable" href="#" target="_self" rel="nofollow" id="slide-28-layer-3" 
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['middle','middle','middle','middle']" data-voffset="['80','73','73','59']" 
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"						 
									data-type="button" 
									data-actions=''
									data-responsive_offset="on" 
									data-responsive="off"
									data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(21,21,21);bg:rgba(255,255,255,1);"}]'
									data-textAlign="['inherit','inherit','inherit','inherit']"
									data-paddingtop="[2,2,2,2]"
									data-paddingright="[20,20,20,20]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[20,20,20,20]">READ MORE </a>
							</li>
						</ul>
						<div class="tp-bannertimer tp-bottom"></div>
					</div>
				</div><!-- END REVOLUTION SLIDER -->
			</div><!-- Slider Section /- -->
			
			<!-- Trending Section -->
			<div class="container-fluid no-left-padding no-right-padding trending-section">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>Trending</h3>
					</div><!-- Section Header /- -->
					<div class="trending-carousel">
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Art">Art</a></span>
									<h3 class="entry-title"><a href="#">A penguin bicycled behind an escalator</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Travel">TRAVEL</a></span>
									<h3 class="entry-title"><a href="#">There was a legend about the well in the garden</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Nature">NATURE</a></span>
									<h3 class="entry-title"><a href="#">The entrance to the tunnel was his only way out</a></h3>
								</div>
							</div>
						</div>
						<div class="type-post">
							<div class="entry-cover"><a href="#"><img src="http://via.placeholder.com/270x220" alt="Trending" /></a></div>
							<div class="entry-content">
								<div class="entry-header">
									<span><a href="#" title="Nature">NATURE</a></span>
									<h3 class="entry-title"><a href="#">He was going back to a place he'd hoped</a></h3>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- Trending Section /- -->
			
			<!-- Page Content -->
			<div class="container-fluid no-left-padding no-right-padding page-content">
				<!-- Container -->
				<div class="container">
					<div class="row">
						<!-- Content Area -->
						<div class="col-lg-8 col-md-6 content-area">
							<!-- Row -->
							<div class="row blog-masonry-list">
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x493" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Travel">Travel</a></span>
												<h3 class="entry-title"><a href="#" title="Charming Evening Field">Charming Evening Field</a></h3>
											</div>								
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Nature">Nature</a></span>
												<h3 class="entry-title"><a href="#" title="Cliff Sunset Sea View">Cliff Sunset Sea View</a></h3>
											</div>
											<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x370" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="TRAVEL">TRAVEL</a></span>
												<h3 class="entry-title"><a href="#" title="Boat Trip to Mediterranean">Boat Trip to Mediterranean</a></h3>
											</div>
											<p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="TRAVEL">TRAVEL</a></span>
												<h3 class="entry-title"><a href="#" title="Beautiful Rio de Janeiro">Beautiful Rio de Janeiro</a></h3>
											</div>
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="entry-title"><a href="#" title="Trendy Summer Fashion">Trendy Summer Fashion</a></h3>
											</div>
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x555" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="TRAVEL">TRAVEL</a></span>
												<h3 class="entry-title"><a href="#" title="Hammock Camping Tips">Hammock Camping Tips</a></h3>
											</div>
											<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x555" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="TECHNOLOGY">TECHNOLOGY</a></span>
												<h3 class="entry-title"><a href="#" title="Traffic Jams Solved">Traffic Jams Solved</a></h3>
											</div>
											<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12 col-sm-6 blog-masonry-box">
									<div class="type-post">
										<div class="entry-cover">
											<div class="post-meta">
												<span class="byline">by <a href="#" title="Indesign">inDesign</a></span>
												<span class="post-date"><a href="#">MARCH 17, 2017</a></span>
											</div>
											<a href="#"><img src="http://via.placeholder.com/370x250" alt="Post" /></a>
										</div>
										<div class="entry-content">
											<div class="entry-header">	
												<span class="post-category"><a href="#" title="LIFESTYLE">LIFESTYLE</a></span>
												<h3 class="entry-title"><a href="#" title="New Fashion Outfits">New Fashion Outfits</a></h3>
											</div>
											<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses...</p>
											<a href="#" title="Read More">Read More</a>
										</div>
									</div>
								</div>
							</div><!-- Row /- -->
						</div><!-- Content Area /- -->
						<!-- Widget Area -->
						<div class="col-lg-4 col-md-6 widget-area">
							<!-- Widget : Latest Post -->
							<aside class="widget widget_latestposts">
								<h3 class="widget-title">Popular Posts</h3>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="Beautiful Landscape View of Rio de Janeiro" href="#">Beautiful Landscape View of Rio de Janeiro</a></h5>
									<span><a href="#">march 14, 2017</a></span>
								</div>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="Enjoy Your Holiday with Adventures" href="#">Enjoy Your Holiday with Adventures</a></h5>
									<span><a href="#">march 15, 2017</a></span>
								</div>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="Best Photography Experience Shooting" href="#">Best Photography Experience Shooting</a></h5>
									<span><a href="#">march 15, 2017</a></span>									
								</div>
								<div class="latest-content">
									<a href="#" title="Recent Posts"><i><img src="http://via.placeholder.com/100x80" class="wp-post-image" alt="blog-1" /></i></a>
									<h5><a title="How to Forecast Your Retirement Savings" href="#">How to Forecast Your Retirement Savings</a></h5>
									<span><a href="#">march 16, 2017</a></span>									
								</div>
							</aside><!-- Widget : Latest Post /- -->
							<!-- Widget : Categories -->
							<aside class="widget widget_categories text-center">
								<h3 class="widget-title">Categories</h3>
								<ul>
									<li><a href="#" title="Nature">Nature</a></li>
									<li><a href="#" title="Technology">Technology</a></li>
									<li><a href="#" title="Travel">Travel</a></li>
									<li><a href="#" title="Sport">Sport</a></li>
									<li><a href="#" title="Lifestyle">Lifestyle</a></li>
								</ul>
							</aside><!-- Widget : Categories /- -->
							<!-- Widget : Instagram -->
							<aside class="widget widget_instagram">
								<h3 class="widget-title">Instagram</h3>
								<ul>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
									<li><a href="#"><img src="http://via.placeholder.com/111x111" alt="Instagram" /></a></li>
								</ul>
							</aside><!-- Widget : Instagram /- -->
							<!-- Widget : Follow Us -->
							<aside class="widget widget_social">
								<h3 class="widget-title">FOLLOW US</h3>
								<ul>
									<li><a href="#" title=""><i class="ion-social-facebook-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-twitter-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-instagram-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-pinterest-outline"></i></a></li>
									<li><a href="#" title=""><i class="ion-social-vimeo-outline"></i></a></li>
								</ul>
							</aside><!-- Widget : Follow Us /- -->
							<!-- Widget : Newsletter -->
							<aside class="widget widget_newsletter">
								<h3 class="widget-title">Newsletter</h3>
								<div class="newsletter-box">
									<i class="ion-ios-email-outline"></i>
									<h4>Sign Up for Newsletter</h4>
									<p>Sign up to receive latest posts and news </p>
									<form>
										<input type="text" class="form-control" placeholder="Your email address" />
										<input type="submit" value="Subscribe Now" />
									</form>
								</div>
							</aside><!-- Widget : Newsletter /- -->
							<!-- Widget : Tags -->
							<aside class="widget widget_tags_cloud">
								<h3 class="widget-title">Tags</h3>
								<div class="tagcloud">
									<a href="#" title="Fashion">Fashion</a>
									<a href="#" title="Travel">Travel</a>
									<a href="#" title="Nature">Nature</a>
									<a href="#" title="Technology">Technology</a>
									<a href="#" title="Sport">Sport</a>
									<a href="#" title="Weather">Weather</a>
									<a href="#" title="Trends">Trends</a>
									<a href="#" title="Lifestyle">Lifestyle</a>
									<a href="#" title="Gear">Gear</a>
								</div>
							</aside><!-- Widget : Tags /- -->
							<!-- Widget : Tranding Post -->
							<aside class="widget widget_tranding_post">
								<h3 class="widget-title">TRENDING Posts</h3>
								<div id="trending-widget" class="carousel slide" data-ride="carousel">
									<div class="carousel-inner">
										<div class="carousel-item active">
											<div class="trnd-post-box">
												<div class="post-cover"><a href="#"><img src="http://via.placeholder.com/345x230" alt="Tranding Post" /></a></div>
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="post-title"><a href="#">New Fashion Collection Show This Weekend in Boston </a></h3>
											</div>
										</div>
										<div class="carousel-item">
											<div class="trnd-post-box">
												<div class="post-cover"><a href="#"><img src="http://via.placeholder.com/345x230" alt="Tranding Post" /></a></div>
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="post-title"><a href="#">New Fashion Collection Show This Weekend in Boston </a></h3>
											</div>
										</div>
										<div class="carousel-item">
											<div class="trnd-post-box">
												<div class="post-cover"><a href="#"><img src="http://via.placeholder.com/345x230" alt="Tranding Post" /></a></div>
												<span class="post-category"><a href="#" title="Lifestyle">Lifestyle</a></span>
												<h3 class="post-title"><a href="#">New Fashion Collection Show This Weekend in Boston </a></h3>
											</div>
										</div>
									</div>
									<ol class="carousel-indicators">
										<li data-target="#trending-widget" data-slide-to="0" class="active"></li>
										<li data-target="#trending-widget" data-slide-to="1"></li>
										<li data-target="#trending-widget" data-slide-to="2"></li>
									</ol>
								</div>
							</aside><!-- Widget : Tranding Post /- -->
						</div><!-- Widget Area /- -->
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Content /- -->
			
		</main>
		
	</div>
	
<?php get_footer();?>