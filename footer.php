<!-- Footer Main -->
      <footer class="container-fluid no-left-padding no-right-padding footer-main">
          <!-- Instagram -->
          <div class="container-fluid no-left-padding no-right-padding instagram-block">
            <ul class="instagram-carousel">

            <?php 
							$args = array('post_type'=>'post', 'posts_per_page'=> 6); 
							$wp_query = new WP_Query($args); 
						  if ( $wp_query->have_posts() ) : 
							  while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
                
            
              <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a></li>
              <!-- <li><a href="#"><img src="http://via.placeholder.com/318x319" alt="Instagram" /></a></li>
              <li><a href="#"><img src="http://via.placeholder.com/318x319" alt="Instagram" /></a></li>
              <li><a href="#"><img src="http://via.placeholder.com/318x319" alt="Instagram" /></a></li>
              <li><a href="#"><img src="http://via.placeholder.com/318x319" alt="Instagram" /></a></li>
              <li><a href="#"><img src="http://via.placeholder.com/318x319" alt="Instagram" /></a></li> -->
              <?php endwhile; 
							wp_reset_postdata(); else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>
            </ul>
          </div><!-- Instagram /- -->
          <!-- Container -->
          

            <?php
              wp_nav_menu( array(
                'theme_location'  => 'socialfooter',
                'container'       => 'div',
                'container_class' => 'container',
                'menu_class'      => 'ftr-social',
                'depth'           => 1
                  ) ); ?>
          <div class="container">
            <div class="copyright">
              <p>Copyright &copy<?php echo date('Y');?> MINIBLOG</p>
            </div>
          </div>
      </footer><!-- Footer Main /- -->

    <?php wp_footer();?>
  </body>
</html>