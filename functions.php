<?php


if ( ! file_exists( get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php' ) ) {
    // File does not exist... return an error.
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
    // File exists... require it.
    require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}


if ( ! function_exists( 'magazine_setup' ) ) :

    function magazine_setup() {

        add_theme_support( 'title-tag' );


        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 825, 510, true );

        register_nav_menus( array(
            'navbar' => esc_html__( 'Navbar Menu', 'magazine' ),
            'socialhead' => esc_html__( 'Social Head', 'magazine' ),
            'headsearch' => esc_html__( 'Search', 'magazine' ),
            'socialfooter' => esc_html__( 'Social Footer', 'magazine' ),
            'signin' => esc_html__( 'Sign In', 'magazine' ),
            'socailsidebar' => esc_html__( 'Follow us', 'magazine' ),
            ) );

        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        add_theme_support( 'post-formats', array(
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );

    }
endif;
add_action( 'after_setup_theme', 'magazine_setup' );


function magazine_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'magazine' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'magazine' ),
        'before_widget' => '<section id="%1s" class="widget %2s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'magazine_widgets_init' );


function magazine_scripts() {

    wp_enqueue_style( 'magazine_settings', get_template_directory_uri() . '/assets/revolution/css/settings.css' );
    wp_enqueue_style( 'magazine_lib', get_template_directory_uri() . '/assets/css/lib.css' );
    wp_enqueue_style( 'magazine_rtl', get_template_directory_uri() . '/assets/css/rtl.css' );
    wp_enqueue_style('magazine_styles', get_stylesheet_uri());

    wp_enqueue_script( 'magazine_jquery', get_template_directory_uri() . '/assets/js/jquery-1.12.4.min.js', array('jquery'), '', true );

    wp_enqueue_script( 'magazine_popper', get_template_directory_uri() . '/assets/js/popper.min.js', array('jquery'), '', true );

    wp_enqueue_script( 'magazine_lib', get_template_directory_uri() . '/assets/js/lib.js', array('jquery'), '', true );

    wp_enqueue_script( 'magazine_jquery_themepunch', get_template_directory_uri() . '/assets/revolution/js/jquery.themepunch.tools.min.js', array('jquery'), '', true );

    wp_enqueue_script( 'magazine_revolution_min', get_template_directory_uri() . '/assets/revolution/js/jquery.themepunch.revolution.min.js', array('jquery'), '', true );

    wp_enqueue_script( 'magazine_revolution_extension_actions', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.actions.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_carousel_min', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.carousel.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_kenburn', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.kenburn.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_layeranimation', get_template_directory_uri() . 'assets/revolution/js/extensions/revolution.extension.layeranimation.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_migration', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.migration.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_navigation', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.navigation.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_parallax', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.parallax.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_slideanims', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.slideanims.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magazine_revolution_video', get_template_directory_uri() . '/assets/revolution/js/extensions/revolution.extension.video.min.js', array('jquery'), '', true );

    wp_enqueue_script( 'magazine_main_function', get_template_directory_uri() . '/assets/js/functions.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'magazine_scripts' );

function new_excerpt_more($more) {
    global $post;
 return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


function posts_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'posts_excerpt_length');
